window.onload = () => {
    var Ctrl = new SuperController();
};

class SuperController {
    
    constructor() {
        this.parent = document.body;
        this.doInitPage();
    }

    doInitPage() {

        if(typeof localStorage.data != 'undefined')
        {
            swal({
                title: 'Please wait!',
                text: 'we redirected',
                icon: 'info',
                buttons: false,
                closeOnEsc : false,
                closeOnClickOutside: false
            });
            this.goToTheCrud(4000);
            return false;
        }
        
        var user = this.parent.querySelector('#user');
        var pass = this.parent.querySelector('#pass');

        var btnNext = this.parent.querySelector('#btnNext');

        btnNext.onclick = () => {
            Object.assign(localStorage,{
                data: JSON.stringify({
                    user: user.value,
                    pass: pass.value
                })
            });

            swal({
                title: 'Welcome!',
                text: 'go to the crud',
                icon: 'success',
                buttons: false,
                closeOnEsc : false,
                closeOnClickOutside: false
            });

            this.goToTheCrud(2000);
        };
    }

    goToTheCrud(time) {
        setTimeout(() => {
            window.location.href = './crud.html';   
        }, time);
    }

}