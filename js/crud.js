const BASE_URL_API = 'http://localhost/content-projects/api-orion-backend/public/api/';
var $table;

window.onload = () => {
    var Ctrl = new CrudController();
};

class CrudController {

    constructor() {
        this.parent = document.body;
        this.companies = this.parent.querySelector('#companies-container');
        this.doInitPage();
    }

    doInitPage() {
        this.getFromApi({
            method: 'GET', 
            url: BASE_URL_API + 'companies/get-companies'
        },this.companies);
    }

    /**
     * 
     * @param {*} obj 
     */
     getFromApi(obj, container) {
        fetch(obj.url,{
            method: obj.method,
            headers: {
                'Content-Type': 'application/json',
                'Authorization': 'Bearer '+btoa(localStorage.data),
            }
        }).then(rs => rs.json()).then(rs => {

            rs.data.map((el,n) => {
                var card = document.createElement('div');
                    card.className = 'ui link card';
                    card.style.width = '20%';
                    card.style.margin = '1em';
                    card.style.display = 'none';
                var image = document.createElement('div');
                    image.className = 'image';
                var img =  document.createElement('img');
                    img.src = el.image;
                var content = document.createElement('div');
                    content.className = 'content';
                var header = document.createElement('span');
                    header.className = 'header';
                    header.innerText = el.name;
                
                container.appendChild(card);
                card.appendChild(image);
                image.appendChild(img);
                card.appendChild(content);
                content.appendChild(header);

                if(rs.data.length == (n+1))
                {
                    $('#companies-container .card').transition({
                        animation : 'zoom',
                        duration  : 800,
                        interval  : 200
                      });
                }

                card.onclick = () => {
                    $('#employee-container').transition('fly left', () => {

                        $('#set-name').text(el.name);
                        $('#set-name').transition('fade');
                        $('#customer-id').val(el.id);
                        $('#customer-img')[0].src = el.image;

                        this.initDatatable();
                    });
                }
            });

            $('#close-icon').on('click', () => {
                $('#set-name').transition('fade',() => {

                    $('#set-name').text('');
                    $('#customer-img')[0].src = '';
                    $('#employee-container').transition('fly left');
                });
            });

        });
    }

    initDatatable() {

        if(!$.fn.DataTable.isDataTable( '#tbl-address' )) 
        {
            $table = $('#tbl-address').DataTable({
                columns: [
                    { title: 'address'},
                    { title: 'lat'},
                    { title: 'lng'},
                    { title: 'options'},
                ],
                ajax: {
                    url: BASE_URL_API + 'companies/get-address',
                    dataSrc: function(rs) {
                        return rs.data.map(el => [
                            el.address,
                            el.lat,
                            el.lng,
                            '<div class="ui fluid mini buttons" data-status="'+el.status+'">'+
                                '<button class="ui green button" onclick="changeStatus(1,'+el.id+')"><i class="check icon"></i></button>'+
                                '<div class="or"></div>'+
                                '<button class="ui red button" onclick="changeStatus(0,'+el.id+')"><i class="remove icon"></i></button>'+
                            +'</div>'
                        ]);
                    },
                    data: function(d) {
                        d.customer = $('#customer-id').val();
                    }
                },
                rowCallback: function(row, setting) {
                    var status = row.querySelector('.buttons');
                        status = status.dataset.status;

                        if(status != 1)
                        {
                            row.classList.add('negative');
                        }
                        else
                        {
                            row.classList.remove('negative');
                        }
                },
            });
        } 
        else
        {
            $table.ajax.reload();
        }    
    }
}

function changeStatus(on,id) {
    fetch(BASE_URL_API + 'companies/change-status/' + id + '/' + on,{
        headers: {
            'Content-Type': 'application/json',
            'Authorization': 'Bearer '+btoa(localStorage.data),
        }
    }).then(rs => rs.json()).then(rs => {
        $table.ajax.reload();
    });
}